const express = require('express');
const bodyParser = require('body-parser');
const config = require('./config');
const logger = require('@overnightjs/logger').Logger;
const {
  getRemoteLogContent,
} = require('./src/controllers/log-viewer.controller');
const routeLogMiddleware = require('./src/middlewares/route-log.middleware');

// @note Express application instance
const applicationInstance = express();

// @note Register middlewares
applicationInstance.use(bodyParser.urlencoded({ extended: false }));
applicationInstance.use(bodyParser.json());
applicationInstance.use(routeLogMiddleware);

// @note Register controller methods to routes
applicationInstance.get('/', getRemoteLogContent);

applicationInstance.listen(config.serviceListenerPort, () =>
  logger.Info(
    `[ProcessViewerService][APP] - LogViewer started listening on port ${config.serviceListenerPort}`
  )
);
