# Remote Log Viewer Service

## Installation & Usage

Simply run `npm install` in the root of the repository and run `npm start`. Configuration can be found in the `config.js` file. Using the `npm run start:dev` you can start the service in development mode where the app will listen to any file changes and will reload the application.
