module.exports = {
  serviceListenerPort: process.env.PORT || 3000,
  bashScriptFileName: 'followLogs',
  remoteHost: '192.168.100.120',
  remoteUser: 'test',
  bufferSize: 4,
  identityFile: 'id_rsa',
};
