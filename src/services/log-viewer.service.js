const config = require('../../config');
const exec = require('child_process').exec;

/**
 * Process script and handle standard error and output
 *
 * @return {string}
 */
const getLogContent = () => {
  const command = `ssh -CqT -o ConnectTimeout=5 -o strictHostKeyChecking=no -i ${config.identityFile} ${config.remoteUser}@${config.remoteHost} < ./scripts/${config.bashScriptFileName}.sh`;

  return exec(
    command,
    { maxBuffer: 1024 * 1024 * config.bufferSize },
    (error, stdout, stderr) => {
      if (error) {
        return {
          error: error.message,
          response: null,
        };
      }

      if (stderr) {
        return {
          error: stderr,
          response: null,
        };
      }

      return {
        error: null,
        response: stdout,
      };
    }
  );
};

module.exports = {
  getLogContent,
};
