const { getLogContent } = require('../services/log-viewer.service');

/**
 * Process bash command to collect log content from
 * the remote machine.
 *
 * @param {Express.request} request
 * @param {Express.response} response
 * @return {Express.response}
 */
const getRemoteLogContent = (request, response) => {
  try {
    const logContent = getLogContent();
    return response.status(200).send(logContent);
  } catch (exception) {
    return response.status(500).send({
      error: `${error.message} - ${error.stack}`,
      response: null,
    });
  }
};

module.exports = {
  getRemoteLogContent,
};
