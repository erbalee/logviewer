const logger = require('@overnightjs/logger').Logger;

module.exports = (request, response, next) => {
  logger.Info(
    `[ProcessViewerService][${request.method}] - Called ${request.path}`
  );
  next();
};
